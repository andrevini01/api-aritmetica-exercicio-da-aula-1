package com.br.itau.controllers;

import com.br.itau.models.Matematica;
import com.br.itau.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import sun.misc.Contended;

import java.util.List;

@Controller
@RestController
@RequestMapping("/matematica")
public class MatematicaController {
    @Autowired
    private MatematicaService matematicaService;

    @PostMapping("soma")
    public int soma(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if (numeros.size() < 2)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário ao menos 2 números");
        return matematicaService.soma(matematica.getNumeros());
    }

    @PostMapping("subtracao")
    public int subtracao(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if (numeros.size() < 2)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário ao menos 2 números");
        return matematicaService.subtracao(matematica.getNumeros());
    }

    @PostMapping("multiplicacao")
    public int multiplicacao(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if (numeros.size() < 2)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário ao menos 2 números");
        return matematicaService.multiplicacao(matematica.getNumeros());
    }

    @PostMapping("divisao")
    public int divisao(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if (numeros.size() < 2)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário ao menos 2 números");
        return matematicaService.divisao(matematica.getNumeros());
    }
}

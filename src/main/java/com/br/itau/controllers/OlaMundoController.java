package com.br.itau.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ola")
public class OlaMundoController {

    @GetMapping("olamundo")
    public String olaMundo(){
        return "Olá Mundo!";
    }

}

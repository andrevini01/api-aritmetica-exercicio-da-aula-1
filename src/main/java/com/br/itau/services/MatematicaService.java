package com.br.itau.services;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class MatematicaService {
    public int soma (List<Integer> numeros){
        int resultado = 0;
        for (int numero : numeros){
            resultado += numero;
        }
        return resultado;
    }

    public int subtracao (List<Integer> numeros){
        int resultado = numeros.get(0);
        for (int i = 1; i < numeros.size(); i++){
            resultado -= numeros.get(i);
        }
        return resultado;
    }

    public int multiplicacao (List<Integer> numeros){
        int resultado = numeros.get(0);
        for (int i = 1; i < numeros.size(); i++){
            resultado *= numeros.get(i);
        }
        return resultado;
    }

    public int divisao (List<Integer> numeros){
        int resultado = numeros.get(0);
        for (int i = 1; i < numeros.size(); i++){
            if (resultado >= numeros.get(i)) {
                resultado /= numeros.get(i);
            }
            else{
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Não é possível dividir um número por outro menor: " + resultado + "/" + numeros.get(i));
            }

        }
        return resultado;
    }
}
